﻿using earnity.Models;
using earnity.Models.Migrations;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace earnity.Factory
{
    public abstract class _BaseFactory<T> where T : BaseItem
    {
        //database context
        protected AppDbContext context;

        protected _BaseFactory(AppDbContext context)
        {
            this.context = context;
        }

        protected abstract DbSet<T> GetDbSet();

        public abstract DbResponse Add(T toAdd);
        public abstract DbResponse Update(T toUpdate);

        public virtual IEnumerable<T> Get(Func<T, bool> selector)
        {
            return GetDbSet().Where(selector).ToList();

        }

        protected int AddAndSaveChanges(T toAdd)
        {
            GetDbSet().Add(toAdd);
            return context.SaveChanges();
        }

        public int Count()
        {
            return GetDbSet().Count();
        }
    }
}
