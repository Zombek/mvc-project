﻿using earnity.Models;
using earnity.Models.Migrations;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace earnity.Factory
{
    public class RestaurantRateFactory : _BaseFactory<RestaurantRate>
    {
        internal RestaurantRateFactory(AppDbContext context) : base(context)
        {
        }

        public override DbResponse Add(RestaurantRate toAdd)
        {
            var response = new DbResponse();

            toAdd.ID = 0;
            toAdd.DateCreated = DateTime.Now;
            toAdd.DateModified = DateTime.Now;

            try
            {
                AddAndSaveChanges(toAdd);
                response.Status = DbTransactionStatus.Success;
                response.ID = toAdd.ID;
                return response;
            }
            catch (Exception)
            {
                response.Status = DbTransactionStatus.TransactionFail;
                response.Message = "Transaction Failed";
                return response;
            }

        }

        public override DbResponse Update(RestaurantRate toUpdate)
        {
           var response = new DbResponse();
           var  beingUpdated = Get(toUpdate.ID);

            if (beingUpdated == null)
            {
                response.Status = DbTransactionStatus.DoesntExists;
                response.Message = "Not Found";
                return response;
            }

            beingUpdated.MinimalAmount = toUpdate.MinimalAmount;
            beingUpdated.AmountToEarn = toUpdate.AmountToEarn;
            beingUpdated.DateModified = DateTime.Now;
            beingUpdated.IsActive = toUpdate.IsActive;
            beingUpdated.IsCurrency = toUpdate.IsCurrency;
            beingUpdated.PlaceID = toUpdate.PlaceID;
            beingUpdated.Place = toUpdate.Place;

            try
            {
                context.SaveChanges();
                response.Status = DbTransactionStatus.Success;
                response.ID = beingUpdated.ID;
                return response;
            }
            catch (Exception)
            {

                response.Status = DbTransactionStatus.TransactionFail;
                response.Message = "Trasaction failed";
                return response;
            }
        }

        public RestaurantRate Get(int id)
        {
            return context.RestaurantRates.Where(r => r.ID == id && r.IsActive == true).SingleOrDefault();
        }

        public ICollection<RestaurantRate> GetRatesForPlace(int id)
        {
            return context.RestaurantRates.Where(r => r.PlaceID == id && r.IsActive == true).ToList();
        }

        protected override DbSet<RestaurantRate> GetDbSet()
        {
            return context.RestaurantRates;
        }
    }
}
