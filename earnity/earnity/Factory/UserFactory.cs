﻿using earnity.Models;
using earnity.Models.Migrations;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System;

namespace earnity.Factory
{
    public class UserFactory : _BaseFactory<User>
    {
        internal UserFactory(AppDbContext context) : base(context)
        {
        }
        /// <summary>
        /// Adds an entity to the database, return response class
        /// </summary>
        /// <param name="toAdd"></param>
        /// <returns></returns>
        /// 


        public override DbResponse Add(User toAdd)
        {
         var response = new DbResponse();

            if (Get(x => x.InstagramToken == toAdd.InstagramToken).Any())
            {
                response.Status = DbTransactionStatus.AlreadyExists;
                response.Message = $"A user with given Instagramtoken exists: {toAdd.InstagramToken}";
                return response;
            }

            toAdd.DateCreated = DateTime.Now;
            toAdd.DateModified = DateTime.Now;
            toAdd.EarnityToken = Guid.NewGuid();
            //TODO: Remove if not needed?
            toAdd.Balance = 0;
            toAdd.ID = 0;

            try
            {
                AddAndSaveChanges(toAdd);
                response.Status = DbTransactionStatus.Success;
                response.Guid = toAdd.EarnityToken;
                return response;
            }
            catch (Exception)
            {
                response.Status = DbTransactionStatus.TransactionFail;
                response.Message = "Transaction Failed";
                return response;
            }
        }

        public override DbResponse Update(User toUpdate)
        {
            var response = new DbResponse();

            var beingUpdated = Get(toUpdate.InstagramToken);

            if (beingUpdated == null)
            {
                response.Status = DbTransactionStatus.NullObject;
                response.Message = "Not found";
                return response;
            }

            beingUpdated.LastName = toUpdate.LastName;
            beingUpdated.Name = toUpdate.Name;
            beingUpdated.NickName = toUpdate.NickName;
            beingUpdated.IsActive = toUpdate.IsActive;
            beingUpdated.DateModified = DateTime.Now;
            beingUpdated.Balance = toUpdate.Balance;

            context.SaveChanges();

            response.Status = DbTransactionStatus.Success;
            response.Guid = beingUpdated.EarnityToken;
            return response;
        }

        public User Get(string id)
        {
            return context.Users.Where(u => u.InstagramToken == id && u.IsActive == true).SingleOrDefault();
        }

        protected override DbSet<User> GetDbSet()
        {
            return context.Users;
        }
    }
}
