﻿using earnity.Models;
using earnity.Models.Migrations;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;

namespace earnity.Factory
{
    public class PlaceCategoryFactory : _BaseFactory<PlaceCategory>
    {
        internal PlaceCategoryFactory(AppDbContext context) : base(context)
        {
        }

        public override DbResponse Add(PlaceCategory toAdd)
        {
            var response = new DbResponse();

            toAdd.DateCreated = DateTime.Now;
            toAdd.DateModified = DateTime.Now;
            toAdd.ID = 0;

            try
            {
                context.Add(toAdd);
                context.SaveChanges();
                response.Status = DbTransactionStatus.Success;
                response.ID = toAdd.ID;
                return response;
            }
            catch (Exception)
            {
                response.Status = DbTransactionStatus.TransactionFail;
                response.Message = "Trasaction failed";
                return response;
            }
        }

        internal ICollection<PlaceCategory> GetPlaceCategories()
        {
            return context.PlaceCategories.Where(p => p.IsActive == true).ToList();
        }

        internal ICollection<PlaceCategory> GetPlaceCategoriesByType(int id)
        {
            return context.PlaceCategories.Where(p => p.IsActive == true && p.PlaceTypeID == id).ToList();
        }

        internal ICollection<PlaceCategory> GetPlaceCategoryByPlaceId(int placeId)
        {
          return context.PlaceCategories.Where(a => a.Places.Any(b => b.ID == placeId)).ToList();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="toUpdate"></param>
        /// <returns></returns>
        public override DbResponse Update(PlaceCategory toUpdate)
        {
            var response = new DbResponse();

            var beingUpdated = Get(toUpdate.ID);

            if (beingUpdated == null)
            {
                response.Status = DbTransactionStatus.NullObject;
                response.Message = "Not found";
                return response;
            }

            beingUpdated.Name = toUpdate.Name;
            beingUpdated.Places = toUpdate.Places;
            beingUpdated.PlaceTypeID = toUpdate.PlaceTypeID;
            beingUpdated.PlaceType = toUpdate.PlaceType;
            beingUpdated.DateModified = DateTime.Now;
            beingUpdated.IsActive = toUpdate.IsActive;

            try
            {
                context.SaveChanges();
                response.Status = DbTransactionStatus.Success;
                response.ID = beingUpdated.ID;
                return response;
            }
            catch (Exception)
            {

                response.Status = DbTransactionStatus.TransactionFail;
                response.Message = "Trasaction failed";
                return response;
            }
        }

        public IEnumerable<PlaceCategory> Search(string search)
        {
            return GetDbSetWithAllIncludes().
                Where(x => x.ID.ToString().ToLower().Contains(search) ||
                           x.Name.ToLower().Contains(search));
        }

        public PlaceCategory Get(int id)
        {
            return context.PlaceCategories.Where(p => p.ID == id && p.IsActive == true).SingleOrDefault();
        }

        protected override DbSet<PlaceCategory> GetDbSet()
        {
            return context.PlaceCategories;
        }

        protected IQueryable<PlaceCategory> GetDbSetWithAllIncludes()
        {
            return GetDbSet();
        }
    }
}
