﻿using earnity.Models;
using earnity.Models.Migrations;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace earnity.Factory
{
    public class PlaceFactory : _BaseFactory<Place>
    {
        internal PlaceFactory(AppDbContext context) : base(context)
        {
        }

        public IEnumerable<Place> Search(string search)
        {
            return GetDbSetWithAllIncludes().
                Where(x => x.ID.ToString().ToLower().Contains(search) ||
                           x.Name.ToLower().Contains(search));
        }

        public override DbResponse Add(Place toAdd)
        {
         var response = new DbResponse();

            toAdd.DateCreated = DateTime.Now;
            toAdd.DateModified = DateTime.Now;
            toAdd.PlaceGuid = Guid.NewGuid();
            toAdd.ID = 0;
            try
            {
                AddAndSaveChanges(toAdd);
                response.Status = DbTransactionStatus.Success;
                response.Guid = toAdd.PlaceGuid;
                return response;
            }
            catch (Exception)
            {
                response.Status = DbTransactionStatus.TransactionFail;
                response.Message = "Transaction failed";
                return response;
            } 
        }

        public ICollection<Place> GetPlaces()
        {
            return context.Places.Where(p => p.IsActive == true).ToList();
        }

        public ICollection<Place> GetPlacesByTypeID(int typeId)
        {
            return context.Places.Where(p => p.IsActive == true && p.PlaceCategory.PlaceTypeID == typeId).ToList();
        }

        public Place Get(Guid guid)
        {
            return context.Places.Where(p => p.IsActive == true && p.PlaceGuid == guid).SingleOrDefault();
        }

        public override DbResponse Update(Place toUpdate)
        {
            var response = new DbResponse();

            var beingUpdated = Get(toUpdate.PlaceGuid);

            if (beingUpdated == null)
            {
                response.Status = DbTransactionStatus.NullObject;
                response.Message = "Place was not found";
                return response;
            }

            beingUpdated.Address = toUpdate.Address;
            beingUpdated.DateModified = DateTime.Now;
            beingUpdated.Description = toUpdate.Description;
            beingUpdated.Description2 = toUpdate.Description2;
            beingUpdated.Email = toUpdate.Email;
            beingUpdated.IsActive = toUpdate.IsActive;
            beingUpdated.Lat = toUpdate.Lat;
            beingUpdated.Lon = toUpdate.Lon;
            beingUpdated.Logo = toUpdate.Logo;
            beingUpdated.Name = toUpdate.Name;
            beingUpdated.NickName = toUpdate.NickName;
            beingUpdated.Password = toUpdate.Password;
            beingUpdated.Photos = toUpdate.Photos;
            beingUpdated.PlaceCategory = toUpdate.PlaceCategory;
            beingUpdated.PlaceCategoryID = toUpdate.PlaceCategoryID;
            beingUpdated.WorkingHours = toUpdate.WorkingHours;


            try
            {
                context.SaveChanges();
                response.Status = DbTransactionStatus.Success;
                response.Guid = beingUpdated.PlaceGuid;
                return response;
            }
            catch (Exception)
            {
                response.Status = DbTransactionStatus.TransactionFail;
                response.Message = "Transaction failed";
                return response;
            }

        }
        protected override DbSet<Place> GetDbSet()
        {
            return context.Places;
        }

        protected IQueryable<Place> GetDbSetWithAllIncludes()
        {
            return GetDbSet();
        }
    }
}
