﻿using System;
using System.Collections.Generic;
using System.Linq;
using earnity.Models;
using earnity.Models.Migrations;
using Microsoft.EntityFrameworkCore;

namespace earnity.Factory
{
    public class PlaceTypeFactory : _BaseFactory<PlaceType>
    {
        internal PlaceTypeFactory(AppDbContext context) : base(context)
        {
        }

        public override DbResponse Add(PlaceType toAdd)
        {
                var response = new DbResponse();
                toAdd.DateCreated = DateTime.Now;
                toAdd.DateModified = DateTime.Now;
                toAdd.ID = 0;

            try
            {

                AddAndSaveChanges(toAdd);
                response.Status = DbTransactionStatus.Success;
                response.ID = toAdd.ID;
                return response;
            }
            catch (Exception)
            {
                response.Status = DbTransactionStatus.TransactionFail;
                response.Message = "Trasaction failed";
                return response;
            }
        }

        internal ICollection<PlaceType> GetPlaceTypes()
        {
            return context.PlaceTypes.Where(p => p.IsActive == true).ToList();
        }

        internal ICollection<PlaceType> GetPlaceTypeByPlace(int id)
        {
            return context.PlaceTypes.Where(p => p.PlaceCategories.Any(c => c.ID == id)).ToList();
        }

        public override DbResponse Update(PlaceType toUpdate)
        {
            var response = new DbResponse();

            var beingUpdated = Get(toUpdate.ID);

            if (beingUpdated == null)
            {
                response.Status = DbTransactionStatus.NullObject;
                response.Message = "Not found";
                return response;
            }

            beingUpdated.Name = toUpdate.Name;
            beingUpdated.IsActive = toUpdate.IsActive;
            beingUpdated.DateModified = DateTime.Now;

            try
            {
                context.SaveChanges();
                response.Status = DbTransactionStatus.Success;
                response.ID = beingUpdated.ID;
                return response;
            }
            catch (Exception)
            {
                response.Status = DbTransactionStatus.TransactionFail;
                response.Message = "Trasaction failed";
                return response;
            }
        }

        public PlaceType Get(int id)
        {
            return context.PlaceTypes.Where(p => p.ID == id && p.IsActive == true).SingleOrDefault();
        }

        protected override DbSet<PlaceType> GetDbSet()
        {
            return context.PlaceTypes;
        }
    }
}
