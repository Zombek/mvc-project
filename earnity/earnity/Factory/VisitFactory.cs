﻿using earnity.Models;
using earnity.Models.Migrations;
using earnity.ViewModels.VisitViewModel;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace earnity.Factory
{
    public class VisitFactory : _BaseFactory<Visit>
    {
        internal VisitFactory (AppDbContext context) : base(context)
        {
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userGuid"></param>
        /// <param name="placeId"></param>
        /// <returns></returns>
        public DbResponse RequestVisitGuid(RequestViewModel request)
        {
            var toAdd = new Visit();
            var response = new DbResponse();

            toAdd.UserGuid = request.UserGuid;
            toAdd.PlaceID = request.PlaceID;
            toAdd.VisitLat = request.VisitLat;
            toAdd.VisitLon = request.VisitLon;
            toAdd.DateCreated = DateTime.Now;
            toAdd.DateModified = DateTime.Now;
            toAdd.VisitGuid = Guid.NewGuid();

            try
            {
                AddAndSaveChanges(toAdd);
                response.Status = DbTransactionStatus.Success;
                response.Guid = toAdd.VisitGuid;
                return response;
            }
            catch (Exception)
            {
                response.Status = DbTransactionStatus.TransactionFail;
                response.Message = "Transaction Failed";
                return response;
            }
        }

        public override DbResponse Update(Visit toUpdate)
        {
            var response = new DbResponse();
            var beingUpdated = Get(toUpdate.VisitGuid);

            if (beingUpdated == null)
            {
                response.Status = DbTransactionStatus.DoesntExists;
                response.Message = "Visit not found";
                return response;
            }

            beingUpdated.DateModified = DateTime.Now;
            beingUpdated.InstagramPhoto = toUpdate.InstagramPhoto;
            beingUpdated.PhotoReciept = toUpdate.PhotoReciept;
            beingUpdated.UserRating = toUpdate.UserRating;

            context.SaveChanges();
            response.Status = DbTransactionStatus.Success;
            response.Guid = beingUpdated.VisitGuid;
            return response;
        }

        private Visit Get(Guid visitGuid)
        {
            return context.Visits.Where(v => v.VisitGuid == visitGuid && v.IsActive == true).SingleOrDefault();
        }

        protected override DbSet<Visit> GetDbSet()
        {
            return context.Visits;
        }

        public override DbResponse Add(Visit toAdd)
        {
            var response = new DbResponse();

            toAdd.DateCreated = DateTime.Now;
            toAdd.DateModified = DateTime.Now;
            toAdd.VisitGuid = Guid.NewGuid();
            toAdd.ID = 0;
            try
            {
                AddAndSaveChanges(toAdd);

                response.Status = DbTransactionStatus.Success;
                response.Guid= toAdd.VisitGuid;
                return response;
            }
            catch (Exception)
            {
                response.Status = DbTransactionStatus.TransactionFail;
                response.Message = "Transaction failed";
                return response;
            }
        }
    }
}
