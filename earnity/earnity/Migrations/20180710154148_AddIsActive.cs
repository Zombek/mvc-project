﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace earnity.Migrations
{
    public partial class AddIsActive : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsActive",
                table: "WithdrawTransactions",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsActive",
                table: "Visits",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "EarnityToken",
                table: "Users",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsActive",
                table: "Users",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsActive",
                table: "PlaceTypes",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsActive",
                table: "Places",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsActive",
                table: "PlacePaymentTransaction",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsActive",
                table: "PlaceCategories",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsActive",
                table: "Photos",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsActive",
                table: "Foundations",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsActive",
                table: "FoundationCategories",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsActive",
                table: "AdminUsers",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsActive",
                table: "WithdrawTransactions");

            migrationBuilder.DropColumn(
                name: "IsActive",
                table: "Visits");

            migrationBuilder.DropColumn(
                name: "EarnityToken",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "IsActive",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "IsActive",
                table: "PlaceTypes");

            migrationBuilder.DropColumn(
                name: "IsActive",
                table: "Places");

            migrationBuilder.DropColumn(
                name: "IsActive",
                table: "PlacePaymentTransaction");

            migrationBuilder.DropColumn(
                name: "IsActive",
                table: "PlaceCategories");

            migrationBuilder.DropColumn(
                name: "IsActive",
                table: "Photos");

            migrationBuilder.DropColumn(
                name: "IsActive",
                table: "Foundations");

            migrationBuilder.DropColumn(
                name: "IsActive",
                table: "FoundationCategories");

            migrationBuilder.DropColumn(
                name: "IsActive",
                table: "AdminUsers");
        }
    }
}
