﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace earnity.Migrations
{
    public partial class ModelUpdate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_RestaurantRate_Places_PlaceID",
                table: "RestaurantRate");

            migrationBuilder.DropPrimaryKey(
                name: "PK_RestaurantRate",
                table: "RestaurantRate");

            migrationBuilder.RenameTable(
                name: "RestaurantRate",
                newName: "RestaurantRates");

            migrationBuilder.RenameIndex(
                name: "IX_RestaurantRate_PlaceID",
                table: "RestaurantRates",
                newName: "IX_RestaurantRates_PlaceID");

            migrationBuilder.AlterColumn<int>(
                name: "UserID",
                table: "Visits",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddColumn<Guid>(
                name: "UserGuid",
                table: "Visits",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "VisitGuid",
                table: "Visits",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddPrimaryKey(
                name: "PK_RestaurantRates",
                table: "RestaurantRates",
                column: "ID");

            migrationBuilder.AddForeignKey(
                name: "FK_RestaurantRates_Places_PlaceID",
                table: "RestaurantRates",
                column: "PlaceID",
                principalTable: "Places",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_RestaurantRates_Places_PlaceID",
                table: "RestaurantRates");

            migrationBuilder.DropPrimaryKey(
                name: "PK_RestaurantRates",
                table: "RestaurantRates");

            migrationBuilder.DropColumn(
                name: "UserGuid",
                table: "Visits");

            migrationBuilder.DropColumn(
                name: "VisitGuid",
                table: "Visits");

            migrationBuilder.RenameTable(
                name: "RestaurantRates",
                newName: "RestaurantRate");

            migrationBuilder.RenameIndex(
                name: "IX_RestaurantRates_PlaceID",
                table: "RestaurantRate",
                newName: "IX_RestaurantRate_PlaceID");

            migrationBuilder.AlterColumn<int>(
                name: "UserID",
                table: "Visits",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_RestaurantRate",
                table: "RestaurantRate",
                column: "ID");

            migrationBuilder.AddForeignKey(
                name: "FK_RestaurantRate_Places_PlaceID",
                table: "RestaurantRate",
                column: "PlaceID",
                principalTable: "Places",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
