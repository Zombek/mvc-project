﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace earnity.Migrations
{
    public partial class EditedVisitProperties : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "PhotoReceipt",
                table: "Visits",
                newName: "PhotoReciept");

            migrationBuilder.AddColumn<int>(
                name: "UserRating",
                table: "Visits",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<double>(
                name: "VisitLat",
                table: "Visits",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<double>(
                name: "VisitLon",
                table: "Visits",
                nullable: false,
                defaultValue: 0.0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "UserRating",
                table: "Visits");

            migrationBuilder.DropColumn(
                name: "VisitLat",
                table: "Visits");

            migrationBuilder.DropColumn(
                name: "VisitLon",
                table: "Visits");

            migrationBuilder.RenameColumn(
                name: "PhotoReciept",
                table: "Visits",
                newName: "PhotoReceipt");
        }
    }
}
