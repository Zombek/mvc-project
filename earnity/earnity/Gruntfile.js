﻿module.exports = function(grunt) {
	grunt.initConfig({
		watch: {
			styles: {
				files: ['wwwroot/app/less/**/*.less'],
				tasks: ['less:dev',  'lesslint'],
				options: {
					nospawn: true
				}
			}
		},



		lesslint: {
			src: ['wwwroot/app/less/*.less'],
			options: {
				less: {
					paths: ['wwwroot/app/less/*.less']
				}
			}
		},

		less: {

			dev: {
				options: {
					compress: false,
					yuicompress: false
				},
				files: {
					'wwwroot/css/main.css': 'wwwroot/app/less/main.less'
				}
			}

		}

	});

	grunt.loadNpmTasks('grunt-contrib-less');
	grunt.loadNpmTasks('grunt-lesslint');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-watch');
	

	grunt.registerTask('default', ['less','watch']);
};