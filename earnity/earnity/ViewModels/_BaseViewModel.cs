﻿using System;
using System.Collections.Generic;

namespace earnity.Views
{
    public abstract class _BaseViewModel<D,T> where T : class where D : class
    {
        public int ID { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateModified { get; set; }
        public bool IsActive { get; set; } = true;

        /// <summary>
        /// returns DbModel from given ViewModel
        /// </summary>
        /// <returns> DbModel class</returns>
        public abstract T ConvertToDbOject();
        /// <summary>
        /// returns ViewModwl from DbClass
        /// </summary>
        /// <param name="toConvert"></param>
        /// <returns>VeiwModel clas</returns>
        public abstract D ConvertToVM(T toConvert);
    }

    public class DataTableData<T>
    {
        public int draw { get; set; }
        public int recordsTotal { get; set; }
        //new
        public int totalDisplayRecords { get; set; }
        public int recordsFiltered { get; set; }
        public List<T> data { get; set; }
    }
}
