﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using earnity.Models;
using earnity.Views;

namespace earnity.ViewModels
{
    public class PlaceDetailsViewModel : _BaseViewModel<PlaceDetailsViewModel, Place>
    {
        [Display(Name = "Nazwa restauracji")]
        [Required(ErrorMessage = "Nazwa jest wymagana")]
        public string Name { get; set; }

        [Display(Name = "Opis")]
        [Required(ErrorMessage = "Opis jest wymagany")]
        public string Description { get; set; }

        [Display(Name = "Krótszy opis")]
        [Required(ErrorMessage = "Krótszy opis jest wymagany")]
        public string Description2 { get; set; }

        [Display(Name = "Logo miejsca")]
        [Required(ErrorMessage = "Logo jest wymagane")]
        public string Logo { get; set; }

        [Display(Name = "Kategoria miejsca")]
        public int PlaceCategoryID { get; set; }

        public virtual PlaceCategory PlaceCategory { get; set; }

        [Display(Name = "Geaographical location - Latitude")]
        [Required(ErrorMessage = "Latitude is required")]
        public double Lat { get; set; }

        [Display(Name = "Geaographical location - Longitude")]
        [Required(ErrorMessage = "Longitude is required")]
        public double Lon { get; set; }

        [Display(Name = "Adres")]
        [Required(ErrorMessage = "Adres jest wymagany")]
        public string Address { get; set; }

        [Display(Name = "Godziny otwarcia")]
        [Required(ErrorMessage = "Godziny otwarcia są wymagane")]
        public string WorkingHours { get; set; }

        public override Place ConvertToDbOject()
        {
            var dbObject = new Place
            {
                ID = ID,
                Name = Name,
                Description = Description,
                Description2 = Description2,
                Logo = Logo,
                PlaceCategoryID = PlaceCategoryID,
                PlaceCategory = PlaceCategory,
                Lat = Lat,
                Lon = Lon,
                Address = Address,
                WorkingHours = WorkingHours,
            };

            return dbObject;
        }

        public override PlaceDetailsViewModel ConvertToVM(Place toConvert)
        {
            ID = toConvert.ID;
            Name = toConvert.Name;
            Description = toConvert.Description;
            Description2 = toConvert.Description2;
            Logo = toConvert.Logo;
            PlaceCategoryID = toConvert.PlaceCategoryID;
            PlaceCategory = toConvert.PlaceCategory;
            Lat = toConvert.Lat;
            Lon = toConvert.Lon;
            Address = toConvert.Address;
            WorkingHours = toConvert.WorkingHours;

            return this;
        }
    }
}
