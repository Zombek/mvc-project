﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using earnity.Models;
using earnity.Views;

namespace earnity.ViewModels
{
    public class PlaceViewModel : _BaseViewModel<PlaceViewModel, Place>
    {
        public Guid PlaceGuid { get; set; }

        public DateTime JoinDate { get; set; }

        [Display(Name = "Name of the place")]
        [Required(ErrorMessage = "Name is required")]
        public string Name { get; set; }

        [Display(Name = "Nickname used for login")]
        [Required(ErrorMessage = "Nickname is required")]
        public string NickName { get; set; }

        //TODO: Should Password be here? 

        [Display(Name = "Password used for login")]
        [Required(ErrorMessage = "Password is required")]
        public string Password { get; set; }

        [Display(Name = "Email")]
        [Required(ErrorMessage = "Email is required")]
        public string Email { get; set; }

        [Display(Name = "Description of the place")]
        [Required(ErrorMessage = "Description is required")]
        public string Description { get; set; }

        [Display(Name = "Second description of the place")]
        [Required(ErrorMessage = "Second description is required")]
        public string Description2 { get; set; }

        [Display(Name = "Logo of the place")]
        [Required(ErrorMessage = "Logo is required")]
        public string Logo { get; set; }

        [Display(Name = "Photos of the place")]
        //[Required(ErrorMessage = "At least one photo is required")]
        public ICollection<Photo> Photos { get; set; }

        [Display(Name = "Assign a category for the place")]
        [Required(ErrorMessage = "Category is required")]
        public int PlaceCategoryID { get; set; }

        [Display(Name = "Assign a category for the place")]
        public virtual PlaceCategory PlaceCategory { get; set; }

        [Display(Name = "Geaographical location - Latitude")]
        [Required(ErrorMessage = "Latitude is required")]
        public double Lat { get; set; }

        [Display(Name = "Geaographical location - Longitude")]
        [Required(ErrorMessage = "Longitude is required")]
        public double Lon { get; set; }

        [Display(Name = "Address of the place")]
        [Required(ErrorMessage = "Address is required")]
        public string Address { get; set; }

        [Display(Name = "Business hours")]
        [Required(ErrorMessage = "Business hours are required")]
        public string WorkingHours { get; set; }

        public double Balance { get; set; }
        public ICollection<RestaurantRate> RestaurantRates { get; set; }



        public override Place ConvertToDbOject()
        {
            var dbObject = new Place
            {
                ID = ID,
                DateCreated = DateCreated,
                DateModified = DateModified,
                IsActive = IsActive,
                PlaceGuid = PlaceGuid,
                JoinDate = JoinDate,
                Name = Name,
                NickName = NickName,
                Description = Description,
                Description2 = Description2,
                Logo = Logo,
                Photos = Photos,
                PlaceCategoryID = PlaceCategoryID,
                PlaceCategory = PlaceCategory,
                Lat = Lat,
                Lon = Lon,
                Address = Address,
                WorkingHours = WorkingHours,
                Balance = Balance,
                RestaurantRates = RestaurantRates
            };

            return dbObject;
        }

        public override PlaceViewModel ConvertToVM(Place toConvert)
        {
            ID = toConvert.ID;
            DateCreated = toConvert.DateCreated;
            DateModified = toConvert.DateModified;
            IsActive = toConvert.IsActive;
            PlaceGuid = toConvert.PlaceGuid;
            JoinDate = toConvert.JoinDate;
            Name = toConvert.Name;
            NickName = toConvert.NickName;
            Description = toConvert.Description;
            Description2 = toConvert.Description2;
            Logo = toConvert.Logo;
            Photos = toConvert.Photos;
            PlaceCategoryID = toConvert.PlaceCategoryID;
            PlaceCategory = toConvert.PlaceCategory;
            Lat = toConvert.Lat;
            Lon = toConvert.Lon;
            Address = toConvert.Address;
            WorkingHours = toConvert.WorkingHours;
            Balance = toConvert.Balance;
            RestaurantRates = toConvert.RestaurantRates;

            return this;
        }
    }
}
