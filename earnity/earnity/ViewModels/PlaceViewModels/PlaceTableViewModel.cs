﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using earnity.Models;
using earnity.Views;

namespace earnity.ViewModels
{
    public class PlaceTableViewModel : ITableViewModel<PlaceTableViewModel,Place>
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Description2 { get; set; }
        public virtual PlaceCategory PlaceCategory { get; set; }

        public PlaceTableViewModel ConvertToTableVM(Place toConvert)
        {
            ID = toConvert.ID.ToString();
            Name = toConvert.Name;
            Description = toConvert.Description;
            Description2 = toConvert.Description2;
            PlaceCategory = toConvert.PlaceCategory;
            return this;
        }

    }
}
