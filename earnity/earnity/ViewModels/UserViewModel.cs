﻿using earnity.Models;
using earnity.Views;
using System;
using System.ComponentModel.DataAnnotations;

namespace earnity.ViewModels
{
    public class UserViewModel : _BaseViewModel<UserViewModel, User>
    {
        [Display(Name ="Instagramtoken")]
        [Required(ErrorMessage = "InstagramToken is required")]
        public string InstagramToken { get; set; }

        public Guid EarnityToken { get; set; }

        [Display(Name = "Name of the user")]
        [Required(ErrorMessage = "Name is required")]
        public string Name { get; set; }

        [Display(Name = "Lastname of the user")]
        [Required(ErrorMessage = "Lastname is required")]
        public string LastName { get; set; }

        [Display(Name = "Nickname of the user")]
        [Required(ErrorMessage = "Nickname is required")]
        public string NickName { get; set; }

        public double Balance { get; set; }


        public override User ConvertToDbOject()
        {
            var databaseObject = new User
            {
                ID = ID,
                DateCreated = DateCreated,
                DateModified = DateModified,
                IsActive = IsActive,
                InstagramToken = InstagramToken,
                EarnityToken = EarnityToken,
                Name = Name,
                LastName = LastName,
                NickName = NickName,
                Balance = Balance
            };
            return databaseObject;
        }

        public override UserViewModel ConvertToVM(User toConvert)
        {
            ID = toConvert.ID;
            DateCreated = toConvert.DateCreated;
            DateModified = toConvert.DateModified;
            IsActive = toConvert.IsActive;
            InstagramToken = toConvert.InstagramToken;
            EarnityToken = toConvert.EarnityToken;
            Name = toConvert.Name;
            LastName = toConvert.LastName;
            NickName = toConvert.NickName;
            Balance = toConvert.Balance;

            return this;
        }
    }
}
