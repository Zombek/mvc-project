﻿using earnity.Models;
using earnity.Views;
using System.ComponentModel.DataAnnotations;

namespace earnity.ViewModels
{
    public class RestaurantRateViewModel : _BaseViewModel<RestaurantRateViewModel, RestaurantRate>
    {
        [Display(Name = "Minimal amount to spend")]
        [Required(ErrorMessage = "Minimal amount is required")]
        public double MinimalAmount { get; set; }
        [Display(Name = "Reward for the spended amount ")]
        [Required(ErrorMessage = "Reward is required")]
        public double AmountToEarn { get; set; }
        [Display(Name = "Option to calculate the reward")]
        [Required(ErrorMessage = "Setting the currency option is required")]
        public bool IsCurrency { get; set; } = true;
        [Display(Name = "Place of the restaurant rate")]
        [Required(ErrorMessage = "place is required")]
        public int PlaceID { get; set; }

        public virtual Place Place { get; set; }

        public override RestaurantRate ConvertToDbOject()
        {
            var dbObject = new RestaurantRate
            {
                ID = ID,
                DateCreated = DateCreated,
                DateModified = DateModified,
                IsActive = IsActive,
                MinimalAmount = MinimalAmount,
                AmountToEarn = AmountToEarn,
                IsCurrency = IsCurrency,
                PlaceID = PlaceID,
                Place = Place
            };
            return dbObject;
        }

        public override RestaurantRateViewModel ConvertToVM(RestaurantRate toConvert)
        {
            ID = toConvert.ID;
            DateCreated = toConvert.DateCreated;
            DateModified = toConvert.DateModified;
            IsActive = toConvert.IsActive;
            IsCurrency = toConvert.IsCurrency;
            MinimalAmount = toConvert.MinimalAmount;
            AmountToEarn = toConvert.AmountToEarn;
            Place = toConvert.Place;
            PlaceID = toConvert.PlaceID;

            return this;
        }
    }
}
