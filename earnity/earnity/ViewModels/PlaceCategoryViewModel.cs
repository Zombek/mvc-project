﻿using earnity.Models;
using earnity.Views;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace earnity.ViewModels
{
    public class PlaceCategoryViewModel : _BaseViewModel<PlaceCategoryViewModel, PlaceCategory>
    {
        [Display(Name = "Nazwa kategorii")]
        [Required(ErrorMessage = "Nazwa jest wymagana")]
        public string Name { get; set; }

        [Display(Name = "Typ miejsc dla kategorii")]
        [Required(ErrorMessage = "Typ jest wymagany")]
        public int PlaceTypeID { get; set; }

        public virtual PlaceType PlaceType { get; set; }

        public virtual ICollection<Place> Places { get; set; }

        public override PlaceCategory ConvertToDbOject()
        {
            var dbObject = new PlaceCategory
            {
                ID = ID,
                DateCreated = DateCreated,
                DateModified = DateModified,
                IsActive = IsActive,
                Name = Name,
                PlaceTypeID = PlaceTypeID,
                PlaceType = PlaceType,
                Places = Places
            };

            return dbObject;
        }

        public override PlaceCategoryViewModel ConvertToVM(PlaceCategory toConvert)
        {
            ID = toConvert.ID;
            DateCreated = toConvert.DateCreated;
            DateModified = toConvert.DateModified;
            IsActive = toConvert.IsActive;
            Name = toConvert.Name;
            PlaceTypeID = toConvert.PlaceTypeID;
            PlaceType = toConvert.PlaceType;
            Places = toConvert.Places;

            return this;
        }
    }
}
