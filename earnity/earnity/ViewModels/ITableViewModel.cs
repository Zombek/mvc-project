﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace earnity.ViewModels
{
    public interface ITableViewModel<out TD, in T> where TD : class where T : class
    {

        TD ConvertToTableVM(T toConvert);
    }
}
