﻿using earnity.Models;
using earnity.Views;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace earnity.ViewModels
{
    public class PlaceTypeViewModel : _BaseViewModel<PlaceTypeViewModel, PlaceType>
    {
        [Display(Name = "Name of the place type")]
        [Required(ErrorMessage = "Name is required")]
        public string Name { get; set; }

        public virtual ICollection<PlaceCategory> PlaceCategories { get; set; }

        public override PlaceType ConvertToDbOject()
        {
            var dbOject = new PlaceType
            {
                ID = ID,
                DateCreated = DateCreated,
                DateModified = DateModified,
                IsActive = IsActive,
                Name = Name,
                PlaceCategories = PlaceCategories
            };

            return dbOject;
        }

        public override PlaceTypeViewModel ConvertToVM(PlaceType toConvert)
        {
            ID = toConvert.ID;
            DateCreated = toConvert.DateCreated;
            DateModified = toConvert.DateModified;
            IsActive = toConvert.IsActive;
            Name = toConvert.Name;
            PlaceCategories = toConvert.PlaceCategories;

            return this;
        }
    }
}
