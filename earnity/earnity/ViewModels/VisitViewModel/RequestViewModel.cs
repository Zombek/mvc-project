﻿using earnity.Models;
using earnity.Views;
using System;
using System.ComponentModel.DataAnnotations;

namespace earnity.ViewModels.VisitViewModel
{
    public class RequestViewModel
    {
        [Display(Name = "User who creates a visit")]
        [Required(ErrorMessage = "User is required")]
        public Guid UserGuid { get; set; }

        [Display(Name = "Place visited")]
        [Required(ErrorMessage = "Place is required")]
        public int PlaceID { get; set; }

        public double VisitLat { get; set; }

        public double VisitLon { get; set; }
    }   
}


