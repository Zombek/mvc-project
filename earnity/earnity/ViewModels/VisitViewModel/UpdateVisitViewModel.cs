﻿using earnity.Models;
using earnity.Views;
using System;
using System.ComponentModel.DataAnnotations;

namespace earnity.ViewModels.VisitViewModel
{
    public class UpdateVisitViewModel : _BaseViewModel<UpdateVisitViewModel, Visit>
    {
        [Required(ErrorMessage = "Viist guid is missing")]
        public Guid VisitGuid { get; set; }

        [Required(ErrorMessage = "Instagram Photo is required")]
        public string InstagramPhoto { get; set; }

        [Required(ErrorMessage = "Photo of the recipe is required")]
        public string PhotoReciept { get; set; }

        public int UserRating { get; set; }


        public override Visit ConvertToDbOject()
        {
            var dbObject = new Visit
            {

                VisitGuid = VisitGuid,
                DateCreated = DateCreated,
                DateModified = DateModified,
                InstagramPhoto = InstagramPhoto,
                PhotoReciept = PhotoReciept,
                UserRating = UserRating
            };

            return dbObject;
        }
        public override UpdateVisitViewModel ConvertToVM(Visit toConvert)
        {
            VisitGuid = toConvert.VisitGuid;
            DateCreated = toConvert.DateCreated;
            DateModified = toConvert.DateModified;
            InstagramPhoto = toConvert.InstagramPhoto;
            PhotoReciept = toConvert.PhotoReciept;
            UserRating = toConvert.UserRating;

            return this;
        }

    }
}
