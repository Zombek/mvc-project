﻿using System;

namespace earnity.Models
{
    public class Visit : BaseItem
    {
        public Guid VisitGuid { get; set; }

        public Guid UserGuid { get; set; }

        public int PlaceID { get; set; }

        public DateTime DateAccepted { get; set; }

        public virtual User User { get; set; }

        public virtual Place Place { get; set; }

        public string HashTags { get; set; }

        public double Reward { get; set; }

        public bool IsAccepted { get; set; }

        public string InstagramPhoto { get; set; }

        public string InstagramPhotoCopy { get; set; }

        public string PhotoReciept { get; set; }

        public double AmountSpent { get; set; }

        /// <summary>
        ///  A scale for the user from 0-5 to grade a visit. Surely we need to expand/specify it somehow
        /// </summary>
        public int UserRating { get; set; }

        /// <summary>
        /// Used to check later if the request for the visit came from a place near the Restaurant or if the user is cheating
        /// </summary>
        public double VisitLat { get; set; }

        public double VisitLon { get; set; }


    }
}
