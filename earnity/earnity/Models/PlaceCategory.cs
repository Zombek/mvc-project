﻿using earnity.ViewModels;
using System.Collections.Generic;

namespace earnity.Models
{
    /// <summary>
    /// Place category e..g sushi food, italian, footbal pitch
    /// </summary>
    public class PlaceCategory : BaseItem
    {
        public string Name { get; set; }

        public int PlaceTypeID { get; set; }

        public virtual PlaceType PlaceType { get; set; }

        public virtual ICollection<Place> Places { get; set; }
    }
}
