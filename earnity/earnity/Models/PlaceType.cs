﻿using earnity.ViewModels;
using System.Collections.Generic;

namespace earnity.Models
{
    /// <summary>
    /// Place type eg restaurant
    /// </summary>
    public class PlaceType : BaseItem
    {
        public string Name { get; set; }

        public virtual ICollection<PlaceCategory> PlaceCategories { get; set; }
    }
}
