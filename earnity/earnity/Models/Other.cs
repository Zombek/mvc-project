﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace earnity.Models
{
    public class Other
    {
    }

    public class DbResponse
    {
        public int Status { get; set; }
        public int ID { get; set; }
        public string Message { get; set; }
        public Guid Guid { get; set; }
    }

    /// <summary>
    /// Set the Transaction Status with this class
    /// </summary>
    public static class DbTransactionStatus
    {
        public static int ValidModel = 2;

        public static int Success = 1;

        public static int NothingChanged = 0;

        public static int TransactionFail = -1;

        public static int NullObject = -2;

        public static int InvalidID = -3;

        public static int AlreadyExists = -4;

        public static int DoesntExists = -5;

        public static int InvalidModel = -6;

        public static int HasChildren = -8;
    }
}
