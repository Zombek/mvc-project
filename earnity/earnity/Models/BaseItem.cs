﻿using System;

namespace earnity.Models
{
    public abstract class BaseItem
    {
        public int ID { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime DateModified { get; set; }

        public bool IsActive { get; set; } = true;
    }
}
