﻿using earnity.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace earnity.Models
{/// <summary>
/// Class used to definie the amount to spend so the user can recieve a reward, added to each place class.
/// </summary>
    public class RestaurantRate : BaseItem
    {
        public RestaurantRate()
        {
        }
        /// <summary>
        /// Sets the Minimum to spend to earn some AmountToEarn
        /// </summary>
        public double MinimalAmount { get; set; }
        /// <summary>
        /// Sets the the Amount to earn, depending on IsCurrency
        /// </summary>
        public double AmountToEarn { get; set; }
        /// <summary>
        /// If false, the PointsToEarn prop will be a % of MinimalAmount of the Rate
        /// </summary>
        public bool IsCurrency { get; set; } = true;

        public int PlaceID { get; set; }

        public virtual Place Place { get; set; }
    }
}
