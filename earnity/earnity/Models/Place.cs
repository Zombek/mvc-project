﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace earnity.Models
{
    /// <summary>
    /// Place type eg restaurant "AAAA"
    /// </summary>
    public class Place : BaseItem
    {

        public Guid PlaceGuid { get; set; }

        /// <summary>
        /// Date when the cooperation started
        /// </summary>
        public DateTime JoinDate { get; set; }

        public string Name { get; set; }

        /// <summary>
        /// for future login
        /// </summary>
        public string NickName { get; set; }

        /// <summary>
        /// For future login
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// For future login
        /// </summary>
        public string Email { get; set; }

        public string Description { get; set; }

        public string Description2 { get; set; }

        public string Logo { get; set; }

        public ICollection<Photo> Photos { get; set; }

        [ForeignKey("PlaceCategoryID")]
        public int PlaceCategoryID { get; set; }

        public virtual PlaceCategory PlaceCategory { get; set; }

        public double Lat { get; set; }

        public double Lon { get; set; }

        public string Address { get; set; }

        public string WorkingHours { get; set; }

        public ICollection<RestaurantRate> RestaurantRates { get; set; }



        /// <summary>
        /// Future
        /// </summary>
        public double Balance { get; set; }
    }
}
