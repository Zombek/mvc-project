﻿using System.Collections.Generic;

namespace earnity.Models
{
    public class Foundation : BaseItem
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public int FoundationCategoryID { get; set; }

        public virtual FoundationCategory FoundationCategory { get; set; }

        public string Statute { get; set; }

        public string Logo { get; set; }

        public string Goals { get; set; }

        public ICollection<Photo> Photos { get; set; }



    }
}
