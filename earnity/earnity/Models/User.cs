﻿using earnity.ViewModels;
using System;

namespace earnity.Models
{
    public class User : BaseItem
    {
        public string InstagramToken { get; set; }

        public Guid EarnityToken { get; set; }

        public string Name { get; set; }

        public string LastName { get; set; }

        public string NickName { get; set; }

        public double Balance { get; set; }
    }
}
