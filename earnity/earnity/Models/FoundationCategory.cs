﻿using System.Collections.Generic;

namespace earnity.Models
{
    public class FoundationCategory : BaseItem
    {
        public string Name { get; set; }

        public virtual ICollection<Foundation> Foundations { get; set; }

    }
}

