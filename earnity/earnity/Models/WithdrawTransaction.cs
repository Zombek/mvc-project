﻿using System;

namespace earnity.Models
{
    public class WithdrawTransaction : BaseItem
    {
        public string GuidCode { get; set; }

        public int UserID { get; set; }

        public virtual User User { get; set; }

        public string BankAccountNumber { get; set; }

        public DateTime SendDate { get; set; }

        public double Amount { get; set; }
    }
}
