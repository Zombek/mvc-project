﻿using Microsoft.EntityFrameworkCore;
using System.Linq;
using earnity.ViewModels;

namespace earnity.Models.Migrations
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions options)
            : base(options)
        {
        }

        public AppDbContext()
        {

        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);


            foreach (var relationship in
                builder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
            {
                relationship.DeleteBehavior = DeleteBehavior.Restrict;
            }

        }


        public DbSet<FoundationCategory> FoundationCategories { get; set; }
        public DbSet<Foundation> Foundations { get; set; }
        public DbSet<Photo> Photos { get; set; }
        public DbSet<RestaurantRate> RestaurantRates { get; set; }



        public DbSet<AdminUser> AdminUsers { get; set; }
        public DbSet<Place> Places { get; set; }
        public DbSet<PlaceCategory> PlaceCategories { get; set; }
        public DbSet<PlaceType> PlaceTypes { get; set; }
        public DbSet<PlacePaymentTransaction> PlacePaymentTransaction { get; set; }

        public DbSet<User> Users { get; set; }
        public DbSet<Visit> Visits { get; set; }

        public DbSet<WithdrawTransaction> WithdrawTransactions { get; set; }

    }
}
