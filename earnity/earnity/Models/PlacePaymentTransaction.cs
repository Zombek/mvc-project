﻿using System;

namespace earnity.Models
{
    public class PlacePaymentTransaction : BaseItem
    {
        public string GuidCode { get; set; }

        public int PlaceID { get; set; }

        public virtual Place User { get; set; }

        public string BankAccountNumber { get; set; }

        public DateTime SendDate { get; set; }

        public double Amount { get; set; }
    }
}
