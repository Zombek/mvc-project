﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using earnity.Factory;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace earnity.Other
{
    public static class ListFiller
    {

        public static void Categories(PlaceCategoryFactory context, dynamic ViewBag, int id = 0)
        {
            var categories = context.GetPlaceCategories();
            if (id > 0)
            {
             var category = context.GetPlaceCategoryByPlaceId(id);

                categories.Concat(category)
                    .Concat(context.GetPlaceCategories().Where(x => x.ID != category.ElementAt(0).ID));
            }
            SelectList categotyList = new SelectList(categories, "ID", "Name");
            ViewBag.Categories = categotyList;
        }

         public static void Types(PlaceTypeFactory context, dynamic ViewBag, int id = 0)
        {
           var types = context.GetPlaceTypes();

            if (id > 0)
            {
                var type = context.GetPlaceTypeByPlace((int)id);
                types.Concat(type)
                    .Concat(context.GetPlaceTypes().Where(x => x.ID !=type.ElementAt(0).ID));
            }
            SelectList typeList = new SelectList(types, "ID", "Name");
            ViewBag.Types = typeList;
        }
    }
}
