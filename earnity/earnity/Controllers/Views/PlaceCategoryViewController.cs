﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using earnity.Factory;
using earnity.Models;
using earnity.Models.Migrations;
using earnity.Other;
using earnity.ViewModels;
using earnity.Views;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace earnity.Controllers.Views
{
    public class PlaceCategoryViewController : Controller
    {

        private readonly PlaceCategoryFactory _context;
        private readonly PlaceTypeFactory _placeTypeContext;
        public PlaceCategoryViewController(AppDbContext dbContext)
        {
            _placeTypeContext = new PlaceTypeFactory(dbContext);
            _context = new PlaceCategoryFactory(dbContext);
        }
        public IActionResult Index()
        {
            return View();
        }

        public JsonResult GetCategoriesJsonData(int draw, int start, int length)
        {
            string search = Request.Query["search[value]"];
            if (search == null)
            {
                search = "";
            }

            int sortColumn = -1;
            string sortDirection = "asc";

            if (length <= 0)
            {
                length = int.MaxValue;
            }

            if (!string.IsNullOrEmpty(Request.Query["order[0][column]"]))
            {
                sortColumn = int.Parse(Request.Query["order[0][column]"]);
            }

            if (!string.IsNullOrEmpty(Request.Query["order[0][dir]"]))
            {
                sortDirection = Request.Query["order[0][column]"];
            }

            var results = _context.Search(search.ToLower());

            if (sortDirection == "asc")
            {
                switch (sortColumn)
                {
                    case 0: results = results.OrderBy(x => x.ID); break;
                    case 1: results = results.OrderBy(x => x.Name); break;
                }
            }
            else
            {
                switch (sortColumn)
                {
                    case 0: results = results.OrderByDescending(x => x.ID); break;
                    case 1: results = results.OrderByDescending(x => x.Name); break;
                }
            }
            int filteredResults = results.Count();

            results = results.Skip(start)
                .Take(length);


            var resultsData = results.ToList()
                .Select(x => new PlaceCategoryViewModel().ConvertToVM(x)).ToList();

            var dataTableData =
                new DataTableData<PlaceCategoryViewModel>
                {
                    draw = draw,
                    recordsTotal = _context.Count(),
                    totalDisplayRecords = _context.Count(),
                    data = resultsData,
                    recordsFiltered = filteredResults
                };

            return Json(dataTableData);
        }

        [HttpPost]
        public ActionResult CategoryDetails (PlaceCategoryViewModel toAdd)
        {
            PlaceCategory categoryObject;
            if (ModelState.IsValid)
            {
                categoryObject = toAdd.ConvertToDbOject();

                if (_context.Get(x => x.ID == toAdd.ID).FirstOrDefault() != null)
                {
                    _context.Update(categoryObject);
                }
                else
                {
                    _context.Add(categoryObject);
                }

                return RedirectToAction("Index");
            }
             
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult CategoryDetails (int id)
        {

            ListFiller.Types(_placeTypeContext, ViewBag, id);
            PlaceCategoryViewModel viewModel = new PlaceCategoryViewModel();
            if (id > 0)
            {
                var categoryObject = _context.Get(x => x.ID == id).FirstOrDefault();
                
                viewModel.ConvertToVM(categoryObject);
                return View(viewModel);
            }
            return View(viewModel);
        }
    }
}
