﻿using System.Linq;
using earnity.Controllers.API;
using earnity.Factory;
using earnity.Models;
using earnity.Models.Migrations;
using earnity.Other;
using earnity.ViewModels;
using earnity.Views;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace earnity.Controllers.Views
{
    public class PlaceViewController : Controller
    {
        private readonly PlaceFactory _context;

        private readonly PlaceCategoryFactory _categoryContext;

        public PlaceViewController(AppDbContext dbContext)
        {
            _categoryContext =  new PlaceCategoryFactory(dbContext);
            _context = new PlaceFactory(dbContext);
        }

        public IActionResult Index()
        {
            return View();
        }

        public JsonResult GetPlacesJsonData(int draw, int start, int length)
        {
            string search = Request.Query["search[value]"];
            if (search == null)
            {
                search = "";
            }

            int sortColumn = -1;
            string sortDirection = "asc";

            if (length <=0)
            {
                length = int.MaxValue;
            }

            if (!string.IsNullOrEmpty(Request.Query["order[0][column]"]))
            {
                sortColumn = int.Parse(Request.Query["order[0][column]"]);
            }

            if (!string.IsNullOrEmpty(Request.Query["order[0][dir]"]))
            {
                sortDirection = Request.Query["order[0][column]"];
            }

            var results = _context.Search(search.ToLower());

            if (sortDirection == "asc")
            {
                switch (sortColumn)
                {
                    case 0: results = results.OrderBy(x => x.ID); break;
                    case 1: results = results.OrderBy(x => x.Name); break;
                }
            }
            else
            {
                switch (sortColumn)
                {
                    case 0: results = results.OrderByDescending(x => x.ID); break;
                    case 1: results = results.OrderByDescending(x => x.Name); break;
                }
            }
            int filteredResults = results.Count();

            results = results.Skip(start)
                .Take(length);


            var resultsData = results.ToList()
                .Select(x => new PlaceTableViewModel().ConvertToTableVM(x)).ToList();

            var dataTableData =
                new DataTableData<PlaceTableViewModel>
                {
                    draw = draw,
                    recordsTotal = _context.Count(),
                    totalDisplayRecords = _context.Count(),
                    data = resultsData,
                    recordsFiltered = filteredResults
                };

            return Json(dataTableData);
        }

        [HttpPost]
        public ActionResult PlaceDetails(PlaceDetailsViewModel toAdd)
        {
            Place placeObject;
            if (ModelState.IsValid)
            {
                placeObject = toAdd.ConvertToDbOject();
                if (_context.Get(x=> x.ID == toAdd.ID) != null)
                {
                    _context.Update(placeObject);
                }
                else
                {
                    _context.Add(placeObject);
                }

              
    

                return RedirectToAction("Index");

            }

            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult PlaceDetails(int id)
        {
            ListFiller.Categories(_categoryContext,ViewBag,(int)id);
           

            if (id > 0)
            {
                var placeObject = _context.Get(x => x.ID == id).FirstOrDefault();
                PlaceDetailsViewModel viewModel = new PlaceDetailsViewModel();
                viewModel.ConvertToVM(placeObject);
                return View(viewModel);
            }
            return  View("PlaceDetails");
        }
    }
}