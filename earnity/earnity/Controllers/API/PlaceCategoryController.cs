﻿using earnity.Factory;
using earnity.Models;
using earnity.Models.Migrations;
using earnity.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace earnity.Controllers.API
{
    [Produces("application/json")]
    [Route("api/PlaceCategory")]
    public class PlaceCategoryController : Controller
    {
        private readonly PlaceCategoryFactory _context;

        public PlaceCategoryController(AppDbContext dbContext)
        {
            _context = new PlaceCategoryFactory(dbContext);
        }

        [HttpGet("GetAllPlaceCategories")]
        public ICollection<PlaceCategoryViewModel> GetAllPlaceCategories()
        {
            var placeCategories = _context.GetPlaceCategories();
            var placeCategoryViewModelList = new List<PlaceCategoryViewModel>();

            foreach (var obj in placeCategories)
            {
                var placeCategoriesModel = new PlaceCategoryViewModel();
                placeCategoriesModel = placeCategoriesModel.ConvertToVM(obj);
                placeCategoryViewModelList.Add(placeCategoriesModel);
            }
            return placeCategoryViewModelList;
        }


        [HttpGet("GetAllPlaceCategoriesByTypeId/{id}")]
        public ICollection<PlaceCategoryViewModel> GetAllPlaceCategoriesById(int id)
        {
            var placeCategories = _context.GetPlaceCategoriesByType(id);
            var placeCategoryViewModelList = new List<PlaceCategoryViewModel>();

            foreach (var obj in placeCategories)
            {
                var placeCategoriesModel = new PlaceCategoryViewModel();
                placeCategoriesModel = placeCategoriesModel.ConvertToVM(obj);
                placeCategoryViewModelList.Add(placeCategoriesModel);
            }
            return placeCategoryViewModelList;
        }

        [HttpGet("GetCategory/{id}")]
        public IActionResult GetCategory(int id)
        {
            var placeCategory = _context.Get(id);
            if (placeCategory != null)
            {
                var placeCategoryViewModel = new PlaceCategoryViewModel();

                placeCategoryViewModel.ConvertToVM(placeCategory);
                return StatusCode(200, placeCategoryViewModel);
            }
            else return StatusCode(400, $"Category with id: {id} doesn't exist");
        }

        [HttpPost("RegisterPlaceCategory")]
        public IActionResult RegisterPlaceCategory([FromBody]PlaceCategoryViewModel placeCategoryToAdd)
        {
            var response = new DbResponse();
            var validationErrors = new List<ValidationResult>();
            var validationContext = new ValidationContext(placeCategoryToAdd,null,null);
            Validator.TryValidateObject(placeCategoryToAdd, validationContext, validationErrors);

            if (ModelState.IsValid)
            {
                var DbOject = placeCategoryToAdd.ConvertToDbOject();
                response = _context.Add(DbOject);

                if (response.Status == DbTransactionStatus.Success)
                {
                    return StatusCode(200, response.ID);
                }
                else
                {
                    return StatusCode(400, response.Message);
                }
            }
            else
            {
                return StatusCode(404, validationErrors);
            }
        }

        [HttpPut("UpdatePlaceCategory")]
        public IActionResult UpdatePlaceCategory([FromBody]PlaceCategoryViewModel placeCategoryToUpdate)
        {
            var response = new DbResponse();
            var validationErrors = new List<ValidationResult>();
            var validationContext = new ValidationContext(placeCategoryToUpdate, null, null);
            Validator.TryValidateObject(placeCategoryToUpdate, validationContext, validationErrors);

            if (ModelState.IsValid)
            {
                var DbObject = placeCategoryToUpdate.ConvertToDbOject();
                response = _context.Update(DbObject);

                if (response.Status == DbTransactionStatus.Success)
                {
                    return StatusCode(200, response.ID);
                }
                else
                {
                    return StatusCode(400, response.Message);
                }
            }
            else
            {
                return StatusCode(404, validationErrors);
            }
        }
    }
}