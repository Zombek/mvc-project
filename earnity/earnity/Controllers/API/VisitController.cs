﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using earnity.Factory;
using earnity.Models;
using earnity.Models.Migrations;
using earnity.ViewModels.VisitViewModel;
using Microsoft.AspNetCore.Mvc;

namespace earnity.Controllers.API
{
    [Produces("application/json")]
    [Route("api/Visit")]
    public class VisitController : Controller
    {
        private readonly VisitFactory _context;
        public VisitController(AppDbContext dbContext)
        {
            _context = new VisitFactory(dbContext);
        }
        
        /// <summary>
        /// Creates a request for a visit with an RequestVisitModel
        /// </summary>
        /// <param name="visitRequest"></param>
        /// <returns>Guid of the visit if everything is ok</returns>
        [HttpPost("RequestVisit")]
        public IActionResult RequestVisit([FromBody] RequestViewModel visitRequest)
        {
            {
                var response = new DbResponse();
                var validationErrors = new List<ValidationResult>();
                var validationContext = new ValidationContext(visitRequest, null, null);
                Validator.TryValidateObject(visitRequest, validationContext, validationErrors);

                if (ModelState.IsValid)
                {
                    response = _context.RequestVisitGuid(visitRequest);

                    if (response.Status == DbTransactionStatus.Success)
                    {
                        return StatusCode(200, response.Guid);
                    }
                    else
                    {
                        return StatusCode(400, response.Message);
                    }
                }
                else
                {
                    return StatusCode(400, validationErrors);
                }
            }
        }

        [HttpPatch("UpdateVisit")]
        public IActionResult UpdateVisit([FromBody] UpdateVisitViewModel visit)
        {
            var response = new DbResponse();
            var validationErrors = new List<ValidationResult>();
            var validationContext = new ValidationContext(visit, null, null);
            Validator.TryValidateObject(visit, validationContext, validationErrors);

            if (ModelState.IsValid)
            {
                var dbObject = visit.ConvertToDbOject();
                response = _context.Update(dbObject);

                if (response.Status == DbTransactionStatus.Success)
                {
                    return StatusCode(200, response.Guid);
                }
                else
                {
                    return StatusCode(400, response.Message);
                }
            }
            else
            {
                return StatusCode(400, validationErrors);
            }
        }
    }
}