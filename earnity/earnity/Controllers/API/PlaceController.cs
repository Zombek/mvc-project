﻿using earnity.Factory;
using earnity.Models;
using earnity.Models.Migrations;
using earnity.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace earnity.Controllers.API
{
    [Produces("application/json")]
    [Route("api/Place")]
    public class PlaceController : Controller
    {
        private readonly PlaceFactory _context;
        public PlaceController(AppDbContext dbContext)
        {
            _context = new PlaceFactory(dbContext);
        }

        [HttpGet("GetAllPlaces")]
        public ICollection<PlaceViewModel> GetAllPlaces()
        {
            var places = _context.GetPlaces();
            var placeViewModelList = new List<PlaceViewModel>();

            foreach (var obj in places)
            {
               var placeViewModel = new PlaceViewModel();
                placeViewModel = placeViewModel.ConvertToVM(obj);
                placeViewModelList.Add(placeViewModel);
            }

            return placeViewModelList;
        }
        [HttpGet("GetAllPlacesByType/{id}")]
        public ICollection<PlaceViewModel> GetAllPlacesByType(int id)
        {
            var places = _context.GetPlacesByTypeID(id);
            var placeViewModelList = new List<PlaceViewModel>();

            foreach (var obj in places)
            {
                var placeViewModel = new PlaceViewModel();
                placeViewModel = placeViewModel.ConvertToVM(obj);
                placeViewModelList.Add(placeViewModel);
            }

            return placeViewModelList;
        }

        [HttpGet("GetPlace/{guid}")]
        public IActionResult GetPlace(Guid guid)
        {
            var place = _context.Get(guid);
            if (place != null)
            {
                var placeViewModel = new PlaceViewModel();
                placeViewModel = placeViewModel.ConvertToVM(place);

                return StatusCode(200, placeViewModel);
            }

            else return StatusCode(400, $"Place with guid: {guid} doesn't exist");

        }

        [HttpPost("RegisterPlace")]
        public IActionResult RegisterPlace([FromBody]PlaceViewModel placeToAdd)
        {
            var response = new DbResponse();
            var validationErrors = new List<ValidationResult>();
            var validationContext = new ValidationContext(placeToAdd, null, null);
            Validator.TryValidateObject(placeToAdd, validationContext, validationErrors);

            if (ModelState.IsValid)
            {
                var DbOject = placeToAdd.ConvertToDbOject();
                response = _context.Add(DbOject);

                if (response.Status == DbTransactionStatus.Success)
                {
                    return StatusCode(200, response.Guid);
                }
                else
                {
                    return StatusCode(400, response.Message);
                }
            }
            else
            {
                return StatusCode(404, validationErrors);
            }
        }
        [HttpPost("UpdatePlace")]
        public IActionResult UpdatePlace([FromBody]PlaceViewModel placeToUpdate)
        {
            var response = new DbResponse();
            var validationErrors = new List<ValidationResult>();
            var validationContext = new ValidationContext(placeToUpdate, null, null);
            Validator.TryValidateObject(placeToUpdate, validationContext, validationErrors);

            if (ModelState.IsValid)
            {
                var DbObject = placeToUpdate.ConvertToDbOject();
                response = _context.Update(DbObject);

                if (response.Status == DbTransactionStatus.Success)
                {
                    return StatusCode(200, response.Guid);
                }
                else
                {
                    return StatusCode(400, response.Message);
                }
            }
            else
            {
                return StatusCode(404, validationErrors);
            }
        }

    }
}