﻿using earnity.Factory;
using earnity.Models;
using earnity.Models.Migrations;
using earnity.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace earnity.Controllers.API
{
    [Produces("application/json")]
    [Route("api/RestaurantRate/")]
    public class RestaurantRateController : Controller
    {
        private readonly RestaurantRateFactory _context;

        public RestaurantRateController(AppDbContext context)
        {
            _context = new RestaurantRateFactory(context);
        }

        [HttpGet("GetRestaurantRates/{id}")]
        public ICollection<RestaurantRateViewModel> GetRestaurantRates(int id)
        {
            var rates = _context.GetRatesForPlace(id);
            var vmList = new List<RestaurantRateViewModel>();
            foreach (var obj in rates)
            {
                var viewModel = new RestaurantRateViewModel();
                viewModel = viewModel.ConvertToVM(obj);
                vmList.Add(viewModel);
            }
            return vmList;
        }

        [HttpPost("RegisterRestaurantRate")]
        public IActionResult RegisterRestaurantRate([FromBody] RestaurantRateViewModel rateViewModel)
        {
            var dbResponse = new DbResponse();
            var validationErrors = new List<ValidationResult>();
            var validator = new ValidationContext(rateViewModel, null, null);
            Validator.TryValidateObject(rateViewModel, validator, validationErrors);

            if (ModelState.IsValid)
            {
                var dbObject = rateViewModel.ConvertToDbOject();
                dbResponse = _context.Add(dbObject);

                if (dbResponse.Status == DbTransactionStatus.Success)
                {
                    return StatusCode(200, dbResponse.ID);
                }
                else
                {
                    return StatusCode(400, dbResponse.Message);
                }
            }
            else
            {
                return StatusCode(404, validationErrors);
            }
        }

        [HttpPut("UpdateRestaurantRate")]
        public IActionResult UpdateRestaurantRate([FromBody] RestaurantRateViewModel rateViewModel)
        {
            var dbResponse = new DbResponse();
            var validationErrors = new List<ValidationResult>();
            var validator = new ValidationContext(rateViewModel, null, null);
            Validator.TryValidateObject(rateViewModel, validator, validationErrors);

            if (ModelState.IsValid)
            {
                var dbObject = rateViewModel.ConvertToDbOject();
                dbResponse = _context.Update(dbObject);

                if (dbResponse.Status == DbTransactionStatus.Success)
                {
                    return StatusCode(200, dbResponse.ID);
                }
                else
                {
                    return StatusCode(400, dbResponse.Message);
                }
            }
            else
            {
                return StatusCode(404, validationErrors);
            }
        }
    }
}
