﻿using earnity.Factory;
using earnity.Models;
using earnity.Models.Migrations;
using earnity.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace earnity.Controllers.API
{
    [Produces("application/json")]
    [Route("api/User")]
    public class UserController : Controller
    {
        private readonly UserFactory _context;
        public UserController(AppDbContext dbContext)
        {
            _context = new UserFactory(dbContext);
        }

        [HttpGet("Login/{instagramToken}")]
        public IActionResult LogIn(string instagramToken)
        {
            var user = _context.Get(instagramToken);
            if (user == null)
            {
                return StatusCode(404, "User not found");
            }
            else
            {
                var token = Json(user.EarnityToken);
                return StatusCode(200, token);
            }
        }

        [HttpPost("RegisterUser")]
        public IActionResult RegisterUser([FromBody] UserViewModel userToAdd)
        {
            var response = new DbResponse();
                var validationErrors = new List<ValidationResult>();
                var validationContext = new ValidationContext(userToAdd, null, null);
                Validator.TryValidateObject(userToAdd, validationContext, validationErrors);

            if (ModelState.IsValid)
            {
                var userObject = userToAdd.ConvertToDbOject();
                    response = _context.Add(userObject);

                    if (response.Status == DbTransactionStatus.Success)
                    {
                        return StatusCode(200, response.Guid);
                    }
                    else
                    {
                        return StatusCode(400, response.Message);
                    }
                }
                else
                {
                    return StatusCode(404, validationErrors);
                }
            
        }

        [HttpPut("UpdateUser")]
        public IActionResult UpdateUser([FromBody]UserViewModel UserToUpdate)
        {
            var response = new DbResponse();
            var validationErrors = new List<ValidationResult>();
            var validationContext = new ValidationContext(UserToUpdate, null, null);
            Validator.TryValidateObject(UserToUpdate, validationContext, validationErrors);

            if (ModelState.IsValid)
            {
                var userObject = UserToUpdate.ConvertToDbOject();
                response = _context.Update(userObject);

                if (response.Status == DbTransactionStatus.Success)
                {
                    return StatusCode(200, response.Guid);
                }
                else
                {
                    return StatusCode(400, response.Message);
                }
            }
            else
            {
                return StatusCode(404, validationErrors);
            }


        }
    }
}