﻿using earnity.Factory;
using earnity.Models;
using earnity.Models.Migrations;
using earnity.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace earnity.Controllers.API
{
    [Produces("application/json")]
    [Route("api/PlaceType/")]
    public class PlaceTypeController : Controller
    {
        private readonly PlaceTypeFactory _context;
        public PlaceTypeController(AppDbContext dbContext)
        {
            _context = new PlaceTypeFactory(dbContext);
        }

        [HttpGet("GetAllPlaceTypes")]
        public ICollection<PlaceTypeViewModel> GetAllPlaceTpyes()
        {
            var placeTypes = _context.GetPlaceTypes();
            var placeTypeViewModelList = new List<PlaceTypeViewModel>();

            foreach (var obj in placeTypes)
            {
                var placeTypeViewModel = new PlaceTypeViewModel();
                placeTypeViewModel = placeTypeViewModel.ConvertToVM(obj);
                placeTypeViewModelList.Add(placeTypeViewModel);
            }
           

            return placeTypeViewModelList;
        }


        [HttpGet("GetPlaceTypeByPlace/{id}")]
        public IActionResult GetPlaceType(int id)
        {
            var place = _context.Get(id);
            if (place != null)
            {
                var placeTypeViewModel = new PlaceTypeViewModel();
                placeTypeViewModel = placeTypeViewModel.ConvertToVM(place);

                return StatusCode(200, placeTypeViewModel);
            }

            else return StatusCode(400, $"Place with id: {id} doesn't exist");
        }

        [HttpPost("RegisterPlaceType")]
        public IActionResult RegisterPlaceType([FromBody]PlaceTypeViewModel placeTypeToAdd)
        {
            var response = new DbResponse();
            var validationErrors = new List<ValidationResult>();
            var validationContext = new ValidationContext(placeTypeToAdd, null, null);
            Validator.TryValidateObject(placeTypeToAdd, validationContext, validationErrors);

            if (ModelState.IsValid)
            {
                var DbOject = placeTypeToAdd.ConvertToDbOject();
                response = _context.Add(DbOject);

                if (response.Status == DbTransactionStatus.Success)
                {
                    return StatusCode(200, response.ID);
                }
                else
                {
                    return StatusCode(400, response.Message);
                }
            }
            else
            {
                return StatusCode(404, validationErrors);
            }

        }

        [HttpPut("UpdatePlaceType")]
        public IActionResult UpdatePlaceType([FromBody]PlaceTypeViewModel placeTypetoUpdate)
        {
            var response = new DbResponse();
            var validationErrors = new List<ValidationResult>();
            var validationContext = new ValidationContext(placeTypetoUpdate, null, null);
            Validator.TryValidateObject(placeTypetoUpdate, validationContext, validationErrors);

            if (ModelState.IsValid)
            {
                var DbObject = placeTypetoUpdate.ConvertToDbOject();
                response = _context.Update(DbObject);

                if (response.Status == DbTransactionStatus.Success)
                {
                    return StatusCode(200, response.ID);
                }
                else
                {
                    return StatusCode(400, response.Message);
                }
            }
            else
            {
                return StatusCode(404, validationErrors);
            }

        }
    }
}