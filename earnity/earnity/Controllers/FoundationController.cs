﻿using Microsoft.AspNetCore.Mvc;

namespace earnity.Controllers
{
    /// <summary>
    /// All related to foundation
    /// </summary>
    [Produces("application/json")]
    [Route("api/Foundation")]
    public class FoundationController : Controller
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public string GetFoundationList()
        {
            return "test";
        }

    }
}